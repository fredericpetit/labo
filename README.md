# Labo As Code : Laboratoire libvirt/KVM/QEMU.

## I) Présentation.

Le but est de créer sous GNU/Linux Ubuntu un laboratoire multiple à l'aide de QEMU (virtualisation type 2) / KVM (virtualisation type 1) et l'API libvirt (interface de gestion). La carte réseau physique sera en mode bridge et plusieurs interfaces réseau logiques seront rendues disponibles.

## II) Configuration réseau (avec script bash de création).

Note : la configuration se fera via [Netplan](https://netplan.io/), surcouche à systemd et qui prendra l'avantage sur Network  si celui-ci est installé.

![Netplan](assets/img/netplan1.png)

- `mv /etc/netplan/01-network-manager-all.yaml /etc/netplan/01-network-manager-all.yaml.backup`
- `systemctl stop NetworkManager.service && systemctl disable NetworkManager.service`
- `systemctl enable systemd-networkd.service && systemctl enable systemd-resolved.service`
- `systemctl restart systemd-networkd && systemctl restart systemd-resolved`
- `./create_network.sh`
- `virsh net-list --all`

### Tâche Netplan si besoin.

- `sudo crontab -e`
- `@reboot /usr/sbin/netplan apply`

## III) Éléments du Laboratoire.

| préfixe | fonction |
| ------ | ------ |
| **[init](https://gitlab.com/fredericpetit/labo/-/tree/main/list/init)** | Réseau principal de simulation d'environnement d'entreprise. 1 routeur (iptables sur Ubuntu, en VM) et 2 switchs virtuels  (Open vSwitch sur Ubuntu, sur l'hôte) avec 4 VLANs (1 DMZ et 3 LANs). 1 firewall pfSense et 1 proxy Squid assurent la sécurité. |

Anciens labos :

| préfixe | fonction |
| ------ | ------ |
| **[vcenter](https://gitlab.com/fredericpetit/labo/-/tree/main/list/vcenter)** | Déployer le client web Vsphere sur deux ESXi reliés à un Active Directory afin de pratiquer la clusterisation. |
| **[global](https://gitlab.com/fredericpetit/labo/-/tree/main/list/global)** | Un firewall en tête de pont avec LAN & DMZ afin de pratiquer le filtrage et le routage, et tester la plupart des services communs professionnels. |
| **fresh** (to-do) | des VMs d'OS non configurés prêts à l'emploi. |
| **od** (to-do) | des VMs d'OS customisés avec OverDeploy. |

### Rappel Dépôts par défaut pour GNU/Linux Debian 12 (format DEB822).

```
#------------------------------------------------------------------------------#
#                   Debian Bookworm 12 OFFICIALS REPOS
#------------------------------------------------------------------------------#

### debian
X-Repolib-Name: debian
Types: deb
Enabled: yes
URIs: http://ftp.fr.debian.org/debian
Suites: bookworm
Components: main contrib non-free-firmware non-free

### debian-src
X-Repolib-Name: debian
Types:deb-src
Enabled: yes
URIs: http://ftp.fr.debian.org/debian
Suites: bookworm
Components: main contrib non-free-firmware non-free

### debian
X-Repolib-Name: debian
Types: deb
Enabled: yes
URIs: http://ftp.fr.debian.org/debian
Suites: bookworm-updates
Components: main contrib non-free-firmware non-free

### debian-src
X-Repolib-Name: debian
Types:deb-src
Enabled: yes
URIs: http://ftp.fr.debian.org/debian
Suites: bookworm-updates
Components: main contrib non-free-firmware non-free

### debian
X-Repolib-Name: debian
Types: deb
Enabled: yes
URIs: http://ftp.fr.debian.org/debian
Suites: bookworm-backports
Components: main contrib non-free-firmware non-free

### debian-src
X-Repolib-Name: debian
Types:deb-src
Enabled: yes
URIs: http://ftp.fr.debian.org/debian
Suites: bookworm-backports
Components: main contrib non-free-firmware non-free

### debian
X-Repolib-Name: debian
Types: deb
Enabled: yes
URIs: http://ftp.fr.debian.org/debian-security
Suites: bookworm-security
Components: main contrib non-free-firmware non-free

### debian-src
X-Repolib-Name: debian
Types:deb-src
Enabled: yes
URIs: http://ftp.fr.debian.org/debian-security
Suites: bookworm-security
Components: main contrib non-free-firmware non-free
```

### Rappel Dépôts par défaut pour GNU/Linux Ubuntu 24.04 LTS (format DEB822).

```
#------------------------------------------------------------------------------#
#                   Ubuntu Noble 24.04 OFFICIALS REPOS
#------------------------------------------------------------------------------#

### ubuntu
X-Repolib-Name: ubuntu
Types: deb
Enabled: yes
URIs: http://fr.archive.ubuntu.com/ubuntu
Suites: noble
Components: main restricted multiverse universe

### ubuntu-src
X-Repolib-Name: ubuntu
Types:deb-src
Enabled: yes
URIs: http://fr.archive.ubuntu.com/ubuntu
Suites: noble
Components: main restricted multiverse universe

### ubuntu
X-Repolib-Name: ubuntu
Types: deb
Enabled: yes
URIs: http://fr.archive.ubuntu.com/ubuntu
Suites: noble-updates
Components: main restricted multiverse universe

### ubuntu-src
X-Repolib-Name: ubuntu
Types:deb-src
Enabled: yes
URIs: http://fr.archive.ubuntu.com/ubuntu
Suites: noble-updates
Components: main restricted multiverse universe

### ubuntu
X-Repolib-Name: ubuntu
Types: deb
Enabled: yes
URIs: http://fr.archive.ubuntu.com/ubuntu
Suites: noble-backports
Components: main restricted multiverse universe

### ubuntu-src
X-Repolib-Name: ubuntu
Types:deb-src
Enabled: yes
URIs: http://fr.archive.ubuntu.com/ubuntu
Suites: noble-backports
Components: main restricted multiverse universe

### ubuntu
X-Repolib-Name: ubuntu
Types: deb
Enabled: yes
URIs: http://security.ubuntu.com/ubuntu
Suites: noble-security
Components: main restricted multiverse universe

### ubuntu-src
X-Repolib-Name: ubuntu
Types:deb-src
Enabled: yes
URIs: http://security.ubuntu.com/ubuntu
Suites: noble-security
Components: main restricted multiverse universe
```

### Rappel de procédure Bypass TPM pour Microsoft Windows 11.
- `reg add HKLM\System\Setup\LabConfig /v "BypassTPMCheck" /t REG_DWORD /d 1 /f`
- `reg add HKLM\System\Setup\LabConfig /v "BypassSecureBootCheck" /t REG_DWORD /d 1 /f`
- `reg add HKLM\System\Setup\LabConfig /v "BypassRAMCheck" /t REG_DWORD /d 1 /f`


### Rappel de procédure XRDP.

1. `sudo apt install xrdp -y; sudo adduser xrdp ssl-cert; sudo systemctl enable xrdp; sudo systemctl restart xrdp;`
1. `sudo groupadd tsusers; sudo adduser adminlocal tsusers;`
