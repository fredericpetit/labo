# Banque (Ventoy)

## I) Présentation.
Toute ma liste d'ISOs et programmes importants à avoir !

## II) Inventaire.

### A) Tools.
| header | header |
| ------ | ------ |
|        |        |
|        |        |

### B) GNU/Linux et Unix.
| nom | lien |
| ------ | ------ |
| **Debian 12** DVD | https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-12.1.0-amd64-DVD-1.iso |
| **Debian 12** CD netinstall | https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.1.0-amd64-netinst.iso |
| **Debian 11** DVD | https://cdimage.debian.org/mirror/cdimage/archive/11.7.0/amd64/iso-dvd/debian-11.7.0-amd64-DVD-1.iso |
| **Debian 11** CD netinstall | https://cdimage.debian.org/mirror/cdimage/archive/11.7.0/amd64/iso-cd/debian-11.7.0-amd64-netinst.iso |
| **Ubuntu 22.04** Desktop | https://releases.ubuntu.com/jammy/ubuntu-22.04.2-desktop-amd64.iso |
| **Ubuntu 22.04** Server | https://releases.ubuntu.com/jammy/ubuntu-22.04.2-live-server-amd64.iso |
| **Ubuntu 23.04** Desktop | https://releases.ubuntu.com/lunar/ubuntu-23.04-desktop-amd64.iso |
| **Ubuntu 23.04** Server | https://releases.ubuntu.com/lunar/ubuntu-23.04-live-server-amd64.iso |
| **Fedora 38** | https://download.fedoraproject.org/pub/fedora/linux/releases/38/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-38-1.6.iso |
| **AlmaLinux 9.2** | https://mirror.almalinux.ikoula.com/9.2/isos/x86_64/AlmaLinux-9-latest-x86_64-dvd.iso |
| **CentOS 9** | https://mirror.in2p3.fr/pub/linux/centos-stream/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-dvd1.iso |
| **Proxmox VE 8.0.2** | https://enterprise.proxmox.com/iso/proxmox-ve_8.0-2.iso |
| **pfSense 2.7** | https://frafiles.netgate.com/mirror/downloads/pfSense-CE-2.7.0-RELEASE-amd64.iso.gz |
| **OPNsense 23.1** | https://mirror.vraphim.com/opnsense/releases/23.1/OPNsense-23.1-OpenSSL-dvd-amd64.iso.bz2 |
| **TrueNAS 13.0** | https://download.freenas.org/13.0/STABLE/U5.3/x64/TrueNAS-13.0-U5.3.iso |
| **OpenvSwitch 3.1.2** | https://www.openvswitch.org/releases/openvswitch-3.1.2.tar.gz |
| **VMware ESXi 7** | VMware-VMvisor-Installer-7.0U3n-21930508.x86_64.iso |
| **VMware ESXi 8** | VMware-VMvisor-Installer-8.0U1a-21813344.x86_64.iso |
| **VMware vCenter Appliance ** | VMware-vCenter-Server-Appliance-8.0.1.00300-22088981_OVF10.ova |

### C) Microsoft Windows.
| nom | lien |
| ------ | ------ |
| Windows Desktop 11 | https://www.microsoft.com/fr-fr/software-download/windows11 |
| Windows Desktop 10 | https://www.microsoft.com/fr-fr/software-download/windows10ISO |
| Windows Server 2022 | https://go.microsoft.com/fwlink/p/?LinkID=2195280&clcid=0x40c&culture=fr-fr&country=FR |

## III) Script bash de téléchargement.
- `./create_bank.sh`