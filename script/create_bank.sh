#!/bin/bash
#  _________________________________________
# |                                         | #
# |               create bank               | #
# |_________________________________________| #

# no root, just user
if [[ $USER == "root" ]]; then
	VIRT_USER="$(id -nu 1000)";
else
	VIRT_USER="$USER";
fi

# path
#VIRT_PATH="/home/$VIRT_USER/Documents/virt";
VIRT_PATH="/virtualisation";
if [ ! -d ${VIRT_PATH} ]; then mkdir $VIRT_PATH; fi;
if [ ! -d ${VIRT_PATH}/bank ]; then mkdir ${VIRT_PATH}/bank; fi;
if [ ! -d ${VIRT_PATH}/img ]; then mkdir ${VIRT_PATH}/img; fi;
if [ ! -d ${VIRT_PATH}/network ]; then mkdir ${VIRT_PATH}/network; fi;

# rights
chmod 755 -R ${VIRT_PATH};
chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};

# list
declare -A URL;
URL[ISO_1]="https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-12.1.0-amd64-DVD-1.iso";
URL[ISO_2]="https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.1.0-amd64-netinst.iso";
URL[ISO_3]="https://cdimage.debian.org/mirror/cdimage/archive/11.7.0/amd64/iso-dvd/debian-11.7.0-amd64-DVD-1.iso";
URL[ISO_4]="https://cdimage.debian.org/mirror/cdimage/archive/11.7.0/amd64/iso-cd/debian-11.7.0-amd64-netinst.iso";
URL[ISO_5]="https://releases.ubuntu.com/jammy/ubuntu-22.04.2-desktop-amd64.iso";
URL[ISO_6]="https://releases.ubuntu.com/jammy/ubuntu-22.04.2-live-server-amd64.iso";
URL[ISO_7]="https://releases.ubuntu.com/mantic/ubuntu-23.10-beta-desktop-amd64.iso";
URL[ISO_8]="https://releases.ubuntu.com/mantic/ubuntu-23.10-beta-live-server-amd64.iso";
URL[ISO_9]="https://cdimage.kali.org/kali-2023.3/kali-linux-2023.3-installer-amd64.iso";
URL[ISO_10]="https://download.fedoraproject.org/pub/fedora/linux/releases/38/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-38-1.6.iso";
URL[ISO_11]="https://mirror.almalinux.ikoula.com/9.2/isos/x86_64/AlmaLinux-9-latest-x86_64-dvd.iso";
URL[ISO_12]="https://mirror.in2p3.fr/pub/linux/centos-stream/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-dvd1.iso";
URL[ISO_13]="https://enterprise.proxmox.com/iso/proxmox-ve_8.0-2.iso";
URL[ISO_14]="https://frafiles.netgate.com/mirror/downloads/pfSense-CE-2.7.0-RELEASE-amd64.iso.gz";
URL[ISO_15]="https://mirror.vraphim.com/opnsense/releases/23.1/OPNsense-23.1-OpenSSL-dvd-amd64.iso.bz2";
URL[ISO_16]="https://download.freenas.org/13.0/STABLE/U5.3/x64/TrueNAS-13.0-U5.3.iso";

# loop
cd ${VIRT_PATH}/bank;
for i in ${!URL[@]}; do wget -nc ${URL[$i]}; done;

# rights
chmod 755 -R ${VIRT_PATH};
chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};