#!/bin/bash
#  ____________________________________________
# |                                            | #
# |               create network               | #
# |____________________________________________| #

# no root, just user
if [[ $USER == "root" ]]; then
	VIRT_USER="$(id -nu 1000)";
else
	VIRT_USER="$USER";
fi

# path
#VIRT_PATH="/home/$VIRT_USER/Documents/virt";
VIRT_PATH="/virtualisation";
if [ ! -d ${VIRT_PATH} ]; then mkdir $VIRT_PATH; fi;
if [ ! -d ${VIRT_PATH}/bank ]; then mkdir ${VIRT_PATH}/bank; fi;
if [ ! -d ${VIRT_PATH}/img ]; then mkdir ${VIRT_PATH}/img; fi;
if [ ! -d ${VIRT_PATH}/network ]; then mkdir ${VIRT_PATH}/network; fi;

# rights
chmod 755 -R ${VIRT_PATH};
chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};

# br0
virsh net-undefine br0;
virsh net-destroy br0;
cat <<< '<network>
	<name>br0</name>
	<forward mode="bridge"/>
	<bridge name="br0"/>
</network>' > ${VIRT_PATH}/network/br0.xml;
virsh net-define --file ${VIRT_PATH}/network/br0.xml;
virsh net-start br0 && virsh net-autostart br0;

# lan0
virsh net-undefine lan0;
virsh net-destroy lan0;
cat <<< '<network>
	<name>lan0</name>
	<bridge name="lan0" stp="on" delay="0"/>
	<domain name="lan0"/>
</network>' > ${VIRT_PATH}/network/lan0.xml;
virsh net-define --file ${VIRT_PATH}/network/lan0.xml;
virsh net-start lan0 && virsh net-autostart lan0;

# lan1
virsh net-undefine lan1;
virsh net-destroy lan1;
cat <<< '<network>
	<name>lan1</name>
	<bridge name="lan1" stp="on" delay="0"/>
	<domain name="lan1"/>
</network>' > ${VIRT_PATH}/network/lan1.xml;
virsh net-define --file ${VIRT_PATH}/network/lan1.xml
virsh net-start lan1 && virsh net-autostart lan1;

# dmz0
virsh net-undefine dmz0;
virsh net-destroy dmz0;
cat <<< '<network>
	<name>dmz0</name>
	<bridge name="dmz0" stp="on" delay="0"/>
	<domain name="dmz0"/>
</network>' > ${VIRT_PATH}/network/dmz0.xml;
virsh net-define --file ${VIRT_PATH}/network/dmz0.xml
virsh net-start dmz0 && virsh net-autostart dmz0;

# dmz1
virsh net-undefine dmz1;
virsh net-destroy dmz1;
cat <<< '<network>
	<name>dmz1</name>
	<bridge name="dmz1" stp="on" delay="0"/>
	<domain name="dmz1"/>
</network>' > ${VIRT_PATH}/network/dmz1.xml;
virsh net-define --file ${VIRT_PATH}/network/dmz1.xml
virsh net-start dmz1 && virsh net-autostart dmz1;

# list
virsh net-list --all;

# rights
chmod 755 -R ${VIRT_PATH};
chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};