#!/bin/bash
#  ____________________________________________
# |                                            | #
# |               create netplan               | #
# |____________________________________________| #

# no root, just user
if [[ $USER == "root" ]]; then
	VIRT_USER="$(id -nu 1000)";
else
	VIRT_USER="$USER";
fi

# path
#VIRT_PATH="/home/$VIRT_USER/Documents/virt";
VIRT_PATH="/virtualisation";
if [ ! -d ${VIRT_PATH} ]; then mkdir $VIRT_PATH; fi;
if [ ! -d ${VIRT_PATH}/bank ]; then mkdir ${VIRT_PATH}/bank; fi;
if [ ! -d ${VIRT_PATH}/img ]; then mkdir ${VIRT_PATH}/img; fi;
if [ ! -d ${VIRT_PATH}/network ]; then mkdir ${VIRT_PATH}/network; fi;

# rights
chmod 755 -R ${VIRT_PATH};
chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};

# network interface
VIRT_NETWORK_INTERFACE=$(ip addr show | grep '2: ' | cut -d: -f2 | sed 's/ //g');
VIRT_NETWORK_FILE="/etc/netplan/01-network-manager-all.yaml";

# netplan
cat <<EOF > $VIRT_NETWORK_FILE
network:
version: 2
renderer: networkd

ethernets:
	${VIRT_NETWORK_INTERFACE}:
		dhcp4: no
		dhcp6: no
		mtu: 1500

bridges:
	br0:
		dhcp4: no
		dhcp6: no
		addresses: [192.168.0.10/24]
		interfaces:
			- ${VIRT_NETWORK_INTERFACE}
		routes:
		- to: 0.0.0.0/0
			via: 192.168.0.254
			metric: 100
			on-link: true
		mtu: 1500
		nameservers:
			addresses: [192.168.0.101]
		parameters:
			stp: true
			forward-delay: 4
EOF
netplan apply;

# rights
chmod 755 -R ${VIRT_PATH};
chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};