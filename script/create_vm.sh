#!/bin/bash
#  _______________________________________
# |                                       | #
# |               create vm               | #
# |_______________________________________| #

# no root, just user
if [[ $USER == "root" ]]; then
	VIRT_USER="$(id -nu 1000)";
else
	VIRT_USER="$USER";
fi

# path
#VIRT_PATH="/home/$VIRT_USER/Documents/virt";
VIRT_PATH="/virtualisation";
if [ ! -d ${VIRT_PATH} ]; then mkdir $VIRT_PATH; fi;
if [ ! -d ${VIRT_PATH}/bank ]; then mkdir ${VIRT_PATH}/bank; fi;
if [ ! -d ${VIRT_PATH}/img ]; then mkdir ${VIRT_PATH}/img; fi;
if [ ! -d ${VIRT_PATH}/network ]; then mkdir ${VIRT_PATH}/network; fi;

# rights
chmod 755 -R ${VIRT_PATH};
chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};

# variables generic
PATH_DISK="${VIRT_PATH}/img";
PATH_BANK="${VIRT_PATH}/bank";

# variables #100 (fresh desktop)
VM_101="101_TPL_DBN12_Fresh";
OS_101="debian12";
ISO_101="$PATH_BANK/debian-12.1.0-amd64-DVD-1.iso";

VM_102="102_TPL_UBU2204_Fresh";
OS_102="ubuntu22.04";
ISO_102="$PATH_BANK/ubuntu-22.04.2-desktop-amd64.iso";

VM_103="103_TPL_UBU2304_Fresh";
OS_103="ubuntu23.04";
ISO_103="$PATH_BANK/ubuntu-23.04-desktop-amd64.iso";

VM_104="104_TPL_KALI20232_Fresh";
OS_104="ubuntu23.04";
ISO_104="$PATH_BANK/kali-linux-2023.2a-installer-amd64.iso";

VM_105="105_TPL_FED38_GNOME_Fresh";
OS_105="fedora38";
ISO_105="$PATH_BANK/Fedora-Workstation-Live-x86_64-38-1.6.iso";

VM_106="106_TPL_FED38_KDE_Fresh";
OS_106="fedora38";
ISO_106="$PATH_BANK/Fedora-Workstation-Live-x86_64-38-1.6.iso";

# variables #200 (OD desktop)

# variables #300 (fresh server)
VM_301="301_TPL_DBN12_Fresh";
OS_301="debian12";
ISO_301="$PATH_BANK/SERVER_EVAL_x64FRE_fr-fr.iso";

VM_302="302_TPL_UBU2204_Fresh";
OS_302="ubuntu22.04";
ISO_302="$PATH_BANK/ubuntu-22.04.2-live-server-amd64.iso";

vm_303="303_TPL_UBU2304_Fresh";
OS_303="ubuntu23.04";
ISO_303="$PATH_BANK/ubuntu-23.04-live-server-amd64.iso";

VM_304="304_TPL_FED38_Fresh";
OS_304="fedora38";
ISO_304="$PATH_BANK/Fedora-Server-dvd-x86_64-38-1.6.iso";

VM_305="305_TPL_ALMA92_Fresh";
OS_305="almalinux9";
ISO_305="$PATH_BANK/AlmaLinux-9-latest-x86_64-dvd.iso";

VM_306="306_TPL_CENTOS9_Fresh";
OS_306="centos-stream9";
ISO_306="$PATH_BANK/CentOS-Stream-9-latest-x86_64-dvd1.iso";

VM_307="307_TPL_W2022_Fresh";
OS_307="win2k22";
ISO_307="$PATH_BANK/SERVER_EVAL_x64FRE_fr-fr.iso";

# variables #400 (Labo server naked without templating)
VM_401="401_NU_srv-pfsense0";
OS_401="freebsd13.1";
ISO_401="$PATH_BANK/pfSense-CE-2.7.0-RELEASE-amd64.iso";

VM_402="402_NU_srv-opnsense0";
OS_402="freebsd13.1";
ISO_402="$PATH_BANK/OPNsense-23.1-OpenSSL-dvd-amd64.iso";

VM_403="403_NU_srv-proxmox0";
OS_403="debian12";
ISO_403="$PATH_BANK/proxmox-ve_8.0-2.iso";

VM_404="404_NU_srv-nas0";
OS_404="freebsd13.1";
ISO_404="$PATH_BANK/TrueNAS-13.0-U5.3.iso";

VM_405="405_NU_srv-esxi0";
OS_405="generic";
ISO_405="$PATH_BANK/VMware-VMvisor-Installer-8.0U1a-21813344.x86_64.iso";



# funcs
function vm_101() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_101;
	/bin/virsh undefine $VM_101 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_101 \
	--disk path=$PATH_DISK/$VM_101.img,size=40 \
	--memory=2048 \
	--vcpus=2 \
	--network bridge=br0,mac=00:11:22:33:01:01 \
	--os-variant=$OS_101 \
	--console pty,target_type=serial \
	--cdrom $ISO_101 \
	--noautoconsole \
	--boot uefi
}

function vm_102() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_102;
	/bin/virsh undefine $VM_102 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_102 \
	--disk path=$PATH_DISK/$VM_102.img,size=40 \
	--memory=2048 \
	--vcpus=2 \
	--network bridge=br0,mac=00:11:22:33:01:02 \
	--os-variant=$OS_102 \
	--console pty,target_type=serial \
	--cdrom $ISO_102 \
	--noautoconsole \
	--boot uefi
}

function vm_103() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_103;
	/bin/virsh undefine $VM_103 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_103 \
	--disk path=$PATH_DISK/$VM_103.img,size=40 \
	--memory=2048 \
	--vcpus=2 \
	--network bridge=br0,mac=00:11:22:33:01:03 \
	--os-variant=$OS_103 \
	--console pty,target_type=serial \
	--cdrom $ISO_103 \
	--noautoconsole \
	--boot uefi
}

function vm_104() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_104;
	/bin/virsh undefine $VM_104 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_104 \
	--disk path=$PATH_DISK/$VM_104.img,size=40 \
	--memory=2048 \
	--vcpus=2 \
	--network bridge=br0,mac=00:11:22:33:01:04 \
	--os-variant=$OS_104 \
	--console pty,target_type=serial \
	--cdrom $ISO_104 \
	--noautoconsole \
	--boot uefi
}

function vm_105() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_105;
	/bin/virsh undefine $VM_105 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_105 \
	--disk path=$PATH_DISK/$VM_105.img,size=30 \
	--memory=2048 \
	--vcpus=2 \
	--network bridge=br0,mac=00:11:22:33:01:05 \
	--os-variant=$OS_105 \
	--console pty,target_type=serial \
	--cdrom $ISO_105 \
	--noautoconsole \
	--boot uefi
}

function vm_106() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_106;
	/bin/virsh undefine $VM_106 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_106 \
	--disk path=$PATH_DISK/$VM_106.img,size=30 \
	--memory=2048 \
	--vcpus=2 \
	--network bridge=br0,mac=00:11:22:33:01:06 \
	--os-variant=$OS_106 \
	--console pty,target_type=serial \
	--cdrom $ISO_106 \
	--noautoconsole \
	--boot uefi
}

function vm_307() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_307;
	/bin/virsh undefine $VM_307 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_307 \
	--disk path=$PATH_DISK/$VM_307.img,size=50 \
	--memory=4096 \
	--vcpus=2 \
	--network bridge=br0 \
	--os-variant=$OS_307 \
	--console pty,target_type=serial \
	--cdrom $ISO_307 \
	--noautoconsole \
	--boot uefi
}

function vm_401() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_401;
	/bin/virsh undefine $VM_401 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_401 \
	--disk path=$PATH_DISK/$VM_401.img,size=10 \
	--memory=1024 \
	--vcpus=1 \
	--network bridge=br0 \
	--network bridge=lan0 \
	--network bridge=lan1 \
	--os-variant=$OS_401 \
	--console pty,target_type=serial \
	--cdrom $ISO_401 \
	--noautoconsole \
	--boot uefi
}

function vm_402() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_402;
	/bin/virsh undefine $VM_402 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_402 \
	--disk path=$PATH_DISK/$VM_402.img,size=10 \
	--memory=1024 \
	--vcpus=1 \
	--network bridge=br0 \
	--network bridge=lan0 \
	--network bridge=lan1 \
	--os-variant=$OS_402 \
	--console pty,target_type=serial \
	--cdrom $ISO_402 \
	--noautoconsole \
	--boot uefi
}

function vm_403() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_403;
	/bin/virsh undefine $VM_403 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_403 \
	--disk path=$PATH_DISK/$VM_403.img,size=100 \
	--memory=8192 \
	--vcpus=8 \
	--network bridge=br0 \
	--os-variant=$OS_403 \
	--console pty,target_type=serial \
	--cdrom $ISO_403 \
	--noautoconsole \
	--boot uefi
}

function vm_404() {
	# Stop and undefine the VM
	/bin/virsh destroy $VM_404;
	/bin/virsh undefine $VM_404 --remove-all-storage;
	# option --os-variant list
	virt-install \
	--virt-type kvm \
	--name=$VM_404 \
	--disk path=$PATH_DISK/$VM_404.img,size=100 \
	--memory=8192 \
	--vcpus=8 \
	--network bridge=br0 \
	--os-variant=$OS_404 \
	--console pty,target_type=serial \
	--cdrom $ISO_404 \
	--noautoconsole \
	--boot uefi
}


############
# fw1
############
function vm_fw1@srv-fw1() {
	virsh destroy fw1@srv-fw1 && virsh undefine fw1@srv-fw1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi \
	--name=fw1@srv-fw1 --os-variant=freebsd13.1 \
	--disk path=$PATH_DISK/fw1@srv-fw1.qcow2,size=10 \
	--memory=1024 --vcpus=1 \
	--network bridge=br0,mac=00:11:22:33:05:01 \
	--network bridge=lan0,mac=00:00:00:00:01:01 \
	--network bridge=dmz0,mac=00:00:00:00:02:01 \
	--cdrom $PATH_BANK/pfSense-CE-2.7.0-RELEASE-amd64.iso
}
function vm_fw1@srv-ldap1() {
	virsh destroy fw1@srv-ldap1 && virsh undefine fw1@srv-ldap1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi \
	--name=fw1@srv-ldap1 --os-variant=win2k22 \
	--disk path=$PATH_DISK/fw1@srv-ldap1.qcow2,size=30 \
	--memory=3072 --vcpus=2 \
	--network bridge=lan0,model=e1000e,mac=00:00:00:00:01:02 \
	--cdrom $PATH_BANK/SERVER_EVAL_x64FRE_fr-fr.iso
}
function vm_fw1@clt-admin1() {
	virsh destroy fw1@clt-admin1 && virsh undefine fw1@clt-admin1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi \
	--name=fw1@clt-admin1 --os-variant=win10 \
	--disk path=$PATH_DISK/fw1@clt-admin1.qcow2,size=50 \
	--memory=3072 --vcpus=2 \
	--network bridge=lan0,model=e1000e,mac=00:00:00:00:01:03 \
	--cdrom $PATH_BANK/Win11_22H2_French_x64v2.iso
}
function vm_fw1@clt-user1() {
	virsh destroy fw1@clt-user1 && virsh undefine fw1@clt-user1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi \
	--name=fw1@clt-user1 --os-variant=win11 \
	--disk path=$PATH_DISK/fw1@clt-user1.qcow2,size=50 \
	--memory=3072 --vcpus=2 \
	--network bridge=lan0,model=e1000e,mac=00:00:00:00:01:04 \
	--cdrom $PATH_BANK/Win10_22H2_French_x64v1.iso
}
function vm_fw1@srv-web1() {
	virsh destroy fw1@srv-web1 && virsh undefine fw1@srv-web1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi \
	--name=fw1@srv-web1 --os-variant=debian12 \
	--disk path=$PATH_DISK/fw1@srv-web1.qcow2,size=30 \
	--memory=3072 --vcpus=2 \
	--network bridge=dmz0,mac=00:00:00:00:02:02 \
	--cdrom $PATH_BANK/ubuntu-23.04-live-server-amd64.iso
}



############
# fw2
############
function vm_fw2@srv-fw1() {
	virsh destroy fw2@srv-fw1 && virsh undefine fw2@srv-fw1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi \
	--name=fw2@srv-fw1 --os-variant=freebsd13.1 \
	--disk path=$PATH_DISK/fw2@srv-fw1.qcow2,size=10 \
	--memory=1024 --vcpus=1 \
	--network bridge=br0,mac=00:11:22:33:05:01 \
	--network bridge=lan0,mac=00:00:00:00:01:01 \
	--network bridge=dmz0,mac=00:00:00:00:02:01 \
	--cdrom $PATH_BANK/pfSense-CE-2.7.0-RELEASE-amd64.iso
}
function vm_fw2@srv-ldap1() {
	virsh destroy fw2@srv-ldap1 && virsh undefine fw2@srv-ldap1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi \
	--name=fw2@srv-ldap1 --os-variant=win2k22 \
	--disk path=$PATH_DISK/fw2@srv-ldap1.qcow2,size=20 \
	--memory=3072 --vcpus=2 \
	--network bridge=lan0,model=e1000e,mac=00:00:00:00:01:02 \
	--cdrom $PATH_BANK/SERVER_EVAL_x64FRE_fr-fr.iso
}


############
# vcenter1
############
# note : when ESXi 8 good go with VMware-VMvisor-Installer-8.0U1a-21813344.x86_64.iso
function vm_vcenter1@srv-ldap1() {
	virsh destroy vcenter1@srv-ldap1 && virsh undefine vcenter1@srv-ldap1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi \
	--name=vcenter1@srv-ldap1 --os-variant=win2k22 \
	--disk path=${PATH_DISK}/vcenter1@srv-ldap1.qcow2,size=30 \
	--memory=2048 --vcpus=2 \
	--network bridge=br0,model=e1000e,mac=00:11:22:33:06:01 \
	--cdrom ${PATH_BANK}/SERVER_EVAL_x64FRE_fr-fr.iso
}
function vm_vcenter1@srv-vsphere1() {
	virsh destroy vcenter1@srv-vsphere1 && virsh undefine vcenter1@srv-vsphere1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi,cdrom \
	--name=vcenter1@srv-vsphere1 --os-variant=win2k22 \
	--disk path=${PATH_DISK}/vcenter1@srv-vsphere1.qcow2,size=30 \
	--disk path=${PATH_BANK}/SERVER_EVAL_x64FRE_fr-fr.iso,device=cdrom \
	--disk path=${PATH_BANK}/VMware-VCSA-all-8.0.1-22088981.iso,device=cdrom \
	--memory=2048 --vcpus=2 \
	--network bridge=br0,model=e1000e,mac=00:11:22:33:06:02
}
function vm_vcenter1@srv-esxi1() {
	virsh destroy vcenter1@srv-esxi1 && virsh undefine vcenter1@srv-esxi1 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi -check disk_size=off \
	--name=vcenter1@srv-esxi1 --os-variant=fedora-unknown \
	--disk path=${PATH_DISK}/vcenter1@srv-esxi1.qcow2,format=qcow2,bus=sata,target=sda,size=80 \
	--disk path=${PATH_DISK}/vcenter1@srv-esxi1_DD310.qcow2,format=qcow2,bus=sata,target=sdb,size=320 \
	--memory=15360 --vcpus=6 \
	--network bridge=br0,model=e1000e,mac=00:11:22:33:06:03 \
	--cdrom ${PATH_BANK}/VMware-VMvisor-Installer-7.0U3n-21930508.x86_64.iso
}
function vm_vcenter1@srv-esxi2() {
	virsh destroy vcenter1@srv-esxi2 && virsh undefine vcenter1@srv-esxi2 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi -check disk_size=off \
	--name=vcenter1@srv-esxi2 --os-variant=fedora-unknown \
	--disk path=${PATH_DISK}/vcenter1@srv-esxi2.qcow2,format=qcow2,bus=sata,target=sda,size=80 \
	--memory=8192 --vcpus=6 \
	--network bridge=br0,model=e1000e,mac=00:11:22:33:06:04 \
	--cdrom ${PATH_BANK}/VMware-VMvisor-Installer-7.0U3n-21930508.x86_64.iso
}
function vm_vcenter1@srv-esxi3() {
	virsh destroy vcenter1@srv-esxi3 && virsh undefine vcenter1@srv-esxi3 --remove-all-storage;
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi -check disk_size=off \
	--name=vcenter1@srv-esxi3 --os-variant=fedora-unknown \
	--disk path=${PATH_DISK}/vcenter1@srv-esxi3.qcow2,format=qcow2,bus=sata,target=sda,size=80 \
	--memory=8192 --vcpus=6 \
	--network bridge=br0,model=e1000e,mac=00:11:22:33:06:05 \
	--cdrom ${PATH_BANK}/VMware-VMvisor-Installer-7.0U3n-21930508.x86_64.iso
}

# loop arguments.
while [[ $# -gt 0 ]]; do
	value="$1";
	case $value in
		$(echo "$VM_101"))
			vm_101;
			shift
			;;
		$(echo "$VM_102"))
			vm_102;
			shift
			;;
		$(echo "$VM_103"))
			vm_103;
			shift
			;;
		$(echo "$VM_104"))
			vm_104;
			shift
			;;
		$(echo "$VM_105"))
			vm_105;
			shift
			;;
		$(echo "$VM_106"))
			vm_106;
			shift
			;;
		$(echo "$VM_301"))
			vm_301;
			shift
			;;
		$(echo "$VM_302"))
			vm_302;
			shift
			;;
		$(echo "$VM_303"))
			vm_303;
			shift
			;;
		$(echo "$VM_304"))
			vm_304;
			shift
			;;
		$(echo "$VM_305"))
			vm_305;
			shift
			;;
		$(echo "$VM_306"))
			vm_306;
			shift
			;;
		$(echo "$VM_307"))
			vm_307;
			shift
			;;
		$(echo "$VM_401"))
			vm_401;
			shift
			;;
		$(echo "$VM_402"))
			vm_402;
			shift
			;;
		$(echo "$VM_403"))
			vm_403;
			shift
			;;
		$(echo "$VM_404"))
			vm_404;
			shift
			;;
		$(echo "$VM_405"))
			vm_405;
			shift
			;;
		$(echo "fw1@srv-fw1"))
			vm_fw1@srv-fw1;
			shift
			;;
		$(echo "fw1@srv-ldap1"))
			vm_fw1@srv-ldap1;
			shift
			;;
		$(echo "fw1@clt-admin1"))
			vm_fw1@clt-admin1;
			shift
			;;
		$(echo "fw1@clt-user1"))
			vm_fw1@clt-user1;
			shift
			;;
		$(echo "fw1@srv-web1"))
			vm_fw1@srv-web1;
			shift
			;;
		$(echo "fw2@srv-fw1"))
			vm_fw2@srv-fw1;
			shift
			;;
		$(echo "fw2@srv-ldap1"))
			vm_fw2@srv-ldap1;
			shift
			;;
		$(echo "fw2@srv-router1"))
			vm_fw2@srv-router1;
			shift
			;;
		$(echo "fw2@srv-switch1"))
			vm_fw2@srv-switch1;
			shift
			;;
		$(echo "vcenter1@srv-ldap1"))
			vm_vcenter1@srv-ldap1;
			shift
			;;
		$(echo "vcenter1@srv-vsphere1"))
			vm_vcenter1@srv-vsphere1;
			shift
			;;
		$(echo "vcenter1@srv-esxi1"))
			vm_vcenter1@srv-esxi1;
			shift
			;;
		$(echo "vcenter1@srv-esxi2"))
			vm_vcenter1@srv-esxi2;
			shift
			;;
		$(echo "vcenter1@srv-esxi3"))
			vm_vcenter1@srv-esxi3;
			shift
			;;
		*)
			echo -e "\n unknown parameter: '$value'\n"
			shift
			exit 1
			;;
	esac
done

# rights
chmod 755 -R ${VIRT_PATH};
chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};
