$CSVFile = "$PSscriptRoot\ad.csv";
$CSVData = Import-CSV -Path $CSVFile -Delimiter ";" -Encoding UTF8;
$Domain = "global.intra";

Foreach($Utilisateur in $CSVData){

	$UtilisateurPrenom = $Utilisateur.Prenom # Stocker le prénom de l'utilisateur
	$UtilisateurNom = $Utilisateur.Nom # Stocker le nom de l'utilisateur
	$PrenomLow = $($Utilisateur.Prenom).Substring(0,1).ToLower();
	$NomLow = $($Utilisateur.Nom).ToLower();
	$UtilisateurLogin = $PrenomLow + "." + $NomLow.Replace("'", "").Replace(" ", "").Replace("é", "e").Replace("è", "e"); # Définir ici le login de la forme 1er lettre du prénom . Nom (ex: Jean Moulin -> j.moulin). Pour ça on utilisera la fonction Substring(1er para, 2ème para)

	$UtilisateurEmail = $UtilisateurLogin + "@" + $Domain #Ici modifiez pour associer le login de l'utilisateur pour la partie nom d'utilisateur de l'adresse mail.
	$UtilisateurMotDePasse = "Tssr2023!";
	$UtilisateurOU = $Utilisateur.OU; #stocker la valeur de l'OU de chaque utilisateur
	$UtilisateurFonction = $Utilisateur.Fonction #Stocker la fonction de l'utilisateur

	# Vérification de la présence de l'utilisateur dans l'AD
	If (Get-ADUser -Filter "SamAccountName -eq '$UtilisateurLogin'" -ErrorAction SilentlyContinue) {
		Write-Warning "L'identifiant '$UtilisateurLogin' existe déjà dans l'AD, suppression pour re-création."
		Remove-ADUser -Identity "$UtilisateurLogin" -Confirm:$False;
	}

	New-ADUser -Name "$UtilisateurNom $UtilisateurPrenom" `
		-DisplayName "$UtilisateurNom $UtilisateurPrenom" `
		-GivenName $UtilisateurPrenom `
		-Surname $UtilisateurNom `
		-SamAccountName $UtilisateurLogin `
		-UserPrincipalName "$($UtilisateurLogin)@$($Domain)" `
		-EmailAddress $UtilisateurEmail `
		-Title $UtilisateurFonction `
		-Path "OU=$UtilisateurOU,DC=GLOBAL,DC=INTRA" `
		-AccountPassword(ConvertTo-SecureString $UtilisateurMotDePasse -AsPlainText -Force) `
		-ChangePasswordAtLogon $False `
		-Enabled $True;

	Write-Output "Création de l'utilisateur : $UtilisateurLogin - $UtilisateurPrenom $UtilisateurNom"
}