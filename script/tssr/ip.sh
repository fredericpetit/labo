#!/bin/bash
# ip.sh - détermine la classe d'une adresse IP et sa plage.
# usage : ip.sh

# FUNC : détermine la classe.
# ARG: $1 int, ECHO string / RETURN boolean.
function classe() {
	PREMIER_OCTET=$1;
	if [[ $PREMIER_OCTET -ge 0 ]]; then
		if [[ $PREMIER_OCTET -le 127 ]]; then
			CLASSE="A";
		elif [[ $PREMIER_OCTET -le 191 ]]; then
			CLASSE="B";
		elif [[ $PREMIER_OCTET -le 223 ]]; then
			CLASSE="C";
		elif [[ $PREMIER_OCTET -le 239 ]]; then
			CLASSE="D";
		elif [[ $PREMIER_OCTET -le 255 ]]; then
			CLASSE="E";
		else return 1; fi;
	else return 1; fi;
	echo $CLASSE;
}

# FUNC : détermine la plage.
# ARG: $1 string, ECHO string.
function plage() {
	TOUS_LES_OCTETS=$1;
	if [[ $TOUS_LES_OCTETS =~ (^10\.)|(^127\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^192\.168\.) ]]; then
		echo "privée";
	else echo "publique"; fi;
}

# FUNC : fonction principale et son appel.
function app() {
	TOURNE_EN_BOUCLE=true;
	# loop : toujours.
	while [[ $TOURNE_EN_BOUCLE == true ]]; do
		echo "Indiquez une adresse IP :"; read DATA;
		# test : data vide.
		if [[ $DATA == "" ]]; then echo "Aucune IP indiquée, relancez le script après avoir fait votre choix."; exit 1; fi;
		# test : data format.
		if [[ ! $DATA =~ (^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$) ]]; then echo "IP non valide, relancez le script après avoir effectué une formation chez Akxia."; exit 1; fi;
		DATA_PREMIER_OCTET=$(cut -d '.' -f1 <<< $DATA);
		CLASSE="$(classe $DATA_PREMIER_OCTET)";
		# test : classe définie.
		if [[ $? -eq 0 ]]; then
			echo " -> cette adresse est de classe $CLASSE";
			PLAGE="$(plage $DATA)";
			echo -e "  ... et $PLAGE !\n";
		else echo -e " -> IP Non valide.\n"; fi;
	done;
}
app;
