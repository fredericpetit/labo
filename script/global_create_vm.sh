#!/bin/bash
#  _______________________________________
# |                                       | #
# |               create vm               | #
# |_______________________________________| #
#
# usage : 
#		- global_create_vm.sh --vm=global@creed --status=import
#		- global_create_vm.sh --vm=global@creed --status=new

# params.
declare -A PARAMS;

# test : no root, just user
if [[ $USER == "root" ]]; then PARAMS[USER]="$(id -nu 1000)"; else PARAMS[USER]="$USER"; fi;

# path
PARAMS[PATH]="/virtualisation";
if [ ! -d ${PARAMS[PATH]} ]; then mkdir ${PARAMS[PATH]}; fi;
if [ ! -d ${PARAMS[PATH]}/bank ]; then mkdir ${PARAMS[PATH]}/bank; fi;
if [ ! -d ${PARAMS[PATH]}/img ]; then mkdir ${PARAMS[PATH]}/img; fi;
if [ ! -d ${PARAMS[PATH]}/network ]; then mkdir ${PARAMS[PATH]}/network; fi;

# rights
chmod 755 -R ${PARAMS[PATH]};
chown ${PARAMS[USER]}:${PARAMS[USER]} -R ${PARAMS[PATH]};

# variables generic
PATH_DISK="${PARAMS[PATH]}/img";
PATH_BANK="${PARAMS[PATH]}/bank";

# variables vm
declare -a  VM=("global@commander global_clt_admin1 global@clt_user1 global@clt_user2 global_creed");

VM_global_commander="global@commander";
OS_global_commander="win11";
ISO_global_commander="$PATH_BANK/Win11_22H2_French_x64v2.iso";

VM_global_clt_admin1="global@clt_admin1";
OS_global_clt_admin1="win11";
ISO_global_clt_admin1="$PATH_BANK/Win11_22H2_French_x64v2.iso";

VM_global_clt_user1="global@clt_user1";
OS_global_clt_user1="win10";
ISO_global_clt_user1="$PATH_BANK/Win10_22H2_French_x64v1.iso";

VM_global_clt_user2="global@clt_user2";
OS_global_clt_user2="win11";
ISO_global_clt_user2="$PATH_BANK/Win11_22H2_French_x64v2.iso";

VM_global_mikael="global@mikael";
OS_VM_global_mikael="win2k22";
ISO_VM_global_mikael="$PATH_BANK/SERVER_EVAL_x64FRE_fr-fr.iso";

VM_global_dwight="global@dwight";
OS_global_dwight="debian12";
ISO_global_dwight="$PATH_BANK/debian-12.1.0-amd64-DVD-1.iso";

VM_global_jan="global@jan";
OS_global_jan="win2k22";
ISO_global_jan="$PATH_BANK/SERVER_EVAL_x64FRE_fr-fr.iso";

VM_global_creed="global@creed";
OS_global_creed="debian12";
ISO_global_creed="$PATH_BANK/debian-12.1.0-amd64-DVD-1.iso";

VM_global_jim="global@jim";
OS_global_jim="debian12";
ISO_global_jim="$PATH_BANK/debian-12.1.0-amd64-DVD-1.iso";

VM_global_pam="global@pam";
OS_global_pam="debian12";
ISO_global_pam="$PATH_BANK/debian-12.1.0-amd64-DVD-1.iso";

VM_global_andy="global@andy";
OS_global_andy="debian12";
ISO_global_andy="$PATH_BANK/debian-12.1.0-amd64-DVD-1.iso";

VM_global_angela="global@angela";
OS_global_angela="debian12";
ISO_global_angela="$PATH_BANK/debian-12.1.0-amd64-DVD-1.iso";

VM_global_erin="global@erin";
OS_global_erin="centos-stream9";
ISO_global_erin="$PATH_BANK/CentOS-Stream-9-latest-x86_64-dvd1.iso";



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                          @@ #
# @@               FUNC GENERIC               @@ #
# @@                                          @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# is_in_array_ind().
#	DESCRIPTION : check if value exists in array ind.
#	ARG : $1 string array, $2 string value to search.
#	RETURN : code.
function is_in_array_ind() {
	# definition.
	local array=("${!1}");
	# loop datas.
	for i in "${array[@]}"; do if [[ "$i" == "$2" ]]; then return 0; fi; done;
	return 1;
}



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                  @@ #
# @@               FUNC               @@ #
# @@                                  @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

function vm_global_commander() {
	# Stop and undefine the VM.
	/bin/virsh destroy $VM_global_commander;
	/bin/virsh undefine --nvram $VM_global_commander;
	rm ${PATH_DISK}/${VM_global_commander}.qcow2;
	# option --os-variant list.
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi --check all=off \
	--name=$VM_global_commander --os-variant=$OS_global_commander \
	--disk path=${PATH_DISK}/${VM_global_commander}.qcow2,format=qcow2,size=50 \
	--memory=3072 --vcpus=2 \
	--network bridge=br0,model=e1000e,mac=00:11:22:33:44:02 \
	--cdrom $ISO_global_commander
}

function vm_global_clt_admin1() {
	# Stop and undefine the VM.
	/bin/virsh destroy $VM_global_clt_admin1;
	/bin/virsh undefine --nvram $VM_global_clt_admin1;
	rm ${PATH_DISK}/${VM_global_clt_admin1}.qcow2;
	# option --os-variant list.
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi --check all=off \
	--name=$VM_global_clt_admin1 --os-variant=$OS_global_clt_admin1 \
	--disk path=${PATH_DISK}/${VM_global_clt_admin1}.qcow2,format=qcow2,size=50 \
	--memory=3072 --vcpus=2 \
	--network bridge=lan0,model=e1000e,mac=00:00:00:00:01:02 \
	--cdrom $ISO_global_clt_admin1
}

function vm_global_creed() {
	# Stop and undefine the VM.
	virsh destroy $VM_global_creed;
	virsh undefine --nvram $VM_global_creed;
	rm ${PATH_DISK}/${VM_global_creed}.qcow2;
	# option --os-variant list.
	virt-install \
	--virt-type kvm --noautoconsole --boot uefi --check all=off \
	--name=$VM_global_creed --os-variant=$OS_global_creed \
	--disk path=${PATH_DISK}/${VM_global_creed}.qcow2,format=qcow2,bus=sata,target=sda,size=40 \
	--memory=3072 --vcpus=2 \
	--network bridge=dmz0,mac=00:00:00:00:02:05 \
	--cdrom $ISO_global_creed
}



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                 @@ #
# @@               VAR               @@ #
# @@                                 @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# loop arguments.
while [[ $# -gt 0 ]]; do
	value="$1";
	case $value in
		--vm=*)
			PARAMS[VM]=$(echo $value | sed -e "s#--vm=##g")
			shift
			;;
		--status=*)
			PARAMS[STATUS]=$(echo $value | sed -e "s#--status=##g")
			shift
			;;
		--path=*)
			PARAMS[PATH]=$(echo $value | sed -e "s#--path=##g")
			shift
			;;
		*)
			echo -e "\n unknown parameter: '$value'\n"
			shift
			exit 1
			;;
	esac
done
# some defaults.
if [ -z "${PARAMS[PATH]}" ]; then PARAMS[PATH]="${PWD}/"; fi;

# test : if VM config exists.
if ! is_in_array_ind "VM[@]" "${PARAMS[VM]}"; then exit 1; fi;

# rights
chmod 755 -R ${PARAMS[PATH]};
chown ${PARAMS[USER]}:${PARAMS[USER]} -R ${PARAMS[PATH]};