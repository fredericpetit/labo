### #400 Labo Server NU.

| nom | fonction |
| ------ | ------ |
|        |        |
|        |        |

#### Script libvirt/KVM/QEMU pour Labo Server NU.

- `./create_vm.sh 401_NU_srv-pfsense0`
- `./create_vm.sh 402_NU_srv-opnsense0`
- `./create_vm.sh 403_NU_srv-proxmox0`
- `./create_vm.sh 404_NU_srv-nas0`
- `./create_vm.sh 405_NU_srv-esxi0`