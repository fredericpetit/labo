Notes :
- Les templates _Mini_ incluent le dépôt public GIT _[OD Datas Example](https://gitlab.com/fredericpetit/overdeploy-datas-example)_, mais les templates _Full_ ont besoin du dépôt GIT à accès restreint.
- Si non précisé, les OS Desktop sont sous GNOME par défaut.
- Fedora est la seule distribution RPM-based avec Desktop.

### #100 Templates Desktop Fresh.
| ID | nom | hostname | famille | configuration |
| ------ | ------ | ------ | ------ | ------ |
| #101 | TPL **DBN 12** Fresh | debian | deb | - mode UEFI<br />- DD 40Go<br />- 1 carte Bridge | 
| #102 | TPL **UBU 2204** Fresh | ubuntu | deb | - mode UEFI<br />- DD 40Go<br />- 1 carte Bridge |
| #103 | TPL **UBU 2304** Fresh | ubuntu | deb | - mode UEFI<br />- DD 40Go<br />- 1 carte Bridge |
| #104 | TPL **KALI 20232** Fresh | kali | deb | - mode UEFI<br />- DD 40Go<br />- 1 carte Bridge |
| #105 | TPL **FED 38** (GNOME) Fresh | fedora | rpm | - mode UEFI<br />- DD 30Go<br />- 1 carte Bridge |
| #106 | TPL **FED 38** (KDE) Fresh | fedora | rpm | - mode UEFI<br />- DD 30Go<br />- 1 carte Bridge |

#### Script libvirt/KVM/QEMU.

- `./create_vm.sh 101_TPL_DBN12_Fresh`
- `./create_vm.sh 102_TPL_UBU2204_Fresh`
- `./create_vm.sh 103_TPL_UBU2304_Fresh`
- `./create_vm.sh 104_TPL_KALI20232_Fresh`
- `./create_vm.sh 105_TPL_FED38_GNOME_Fresh`
- `./create_vm.sh 106_TPL_FED38_KDE_Fresh`



### #300 Templates Server Fresh.

| ID | nom | hostname | famille | configuration |
| ------ | ------ | ------ | ------ | ------ |
| #301 | TPL **DBN 12** Fresh | debian | deb | - mode UEFI<br />- DD 40Go<br />- 1 carte Bridge | 
| #302 | TPL **UBU 2204** Fresh | ubuntu | deb | - mode UEFI<br />- DD 40Go<br />- 1 carte Bridge |
| #303 | TPL **UBU 2304** Fresh | ubuntu | deb | - mode UEFI<br />- DD 40Go<br />- 1 carte Bridge |
| #304 | TPL **FED 38** Fresh | fedora | rpm | - mode UEFI<br />- DD 30Go<br />- 1 carte Bridge |
| #305 | TPL **ALMA 92** Fresh | alma | rpm | - mode UEFI<br />- DD 30Go<br />- 1 carte Bridge |
| #306 | TPL **CENTOS 9** Fresh | centos | rpm | - mode UEFI<br />- DD 30Go<br />- 1 carte Bridge |
| #307 | TPL **W2022** Fresh | windows | windows | - mode UEFI<br />- DD 50Go<br />- 1 carte Bridge |

#### Script libvirt/KVM/QEMU.

- `./create_vm.sh 301_TPL_DBN12_Fresh`
- `./create_vm.sh 302_TPL_UBU2204_Fresh`
- `./create_vm.sh 303_TPL_UBU2304_Fresh`
- `./create_vm.sh 304_TPL_FED38_Fresh`
- `./create_vm.sh 305_TPL_ALMA92_Fresh`
- `./create_vm.sh 306_TPL_CENTOS9_Fresh`
- `./create_vm.sh 307_TPL_W2022_Fresh`

