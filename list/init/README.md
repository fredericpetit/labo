# Labo INIT - init.intra.

## I) Inventaires.

### A) Équipements.

| équipement | fonction | équipement branché dessus | adresse IP / réseau | VLAN |
| ------ | ------ | ------ | ------ | ------ |
| R0 Routeur principal | Box internet Freebox | R1 | 192.168.0.254/24<br />(adresse fixe de la passerelle) | |
| R1 Routeur secondaire | VM Ubuntu GNU/Linux<br />avec iptables (dans KVM) | S0 | 192.168.0.101<br />(adresse fixe de la carte physique réelle, bridge sur l'hôte) | |
| S0 Switch Principal | VM Ubuntu GNU/Linux<br />avec Open vSwitch (dans KVM) | S1, S2, S3, S4 | | |
| S1 Switch 1 | VM Ubuntu GNU/Linux<br />avec Open vSwitch (dans KVM) | | 10.0.0.0/27 | 10 |
| S2 Switch 2 | VM Ubuntu GNU/Linux<br />avec Open vSwitch (dans KVM) | | 172.16.0.0/27 | 20 |
| S3 Switch 3 | VM Ubuntu GNU/Linux<br />avec Open vSwitch (dans KVM) | | 192.168.1.0/24 | 30 |
| S4 Switch 4 | VM Ubuntu GNU/Linux<br />avec Open vSwitch (dans KVM) | | 192.168.2.0/24 | 40 |

### B) Réseau.

| réseau | nom | VLAN | fonction |
| ------ | ------ | ------ | ------ |
| 10.0.0.0/27 | dmz-init0 | VLAN 10 | serveurs |
| 172.16.0.0/27 | lan-init0 | VLAN 20 | serveurs |
| 192.168.1.0/24 | lan-init1 | VLAN 30 | clients |
| 192.168.2.0/24 | lan-init2 | VLAN 40 | clients |

## II) Topologie Packet Tracer.

### Essai avec pka2xml (2024-11-21).

#### .pkt to .xml .

```
sudo apt-get install -y git libcrypto++-utils libzip4t64 libre2j-java graphviz;
git clone https://github.com/mircodz/pka2xml;
cd pka2xml;
sudo make dynamic-install;
pka2xml -d init-network.pkt init-network.xml;
```

#### .xml to graph.

```
chmod +x graph.py
python3 graph.py /home/adminlocal/init-network.xml;
```

## III) Configuration réseau  (netplan & libvirt API).

- voir mon outil lab-network.
- voir la vidéo AdrienD.