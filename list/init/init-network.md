# ROUTER 1

```
en
conf t

hostname ROUTER1

int g0/0
ip add 192.168.0.254 255.255.255.0
no sh

end
w
```

# ROUTER 2

```
en
conf t

hostname ROUTER2

int g0/0
ip add 192.168.0.50 255.255.255.0
no sh

int g0/1
ip add 192.168.1.50 255.255.255.0
no sh

end
w
```