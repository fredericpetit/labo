# Labo vcenter - vcenter.intra

## I) Inventaire.

| hostname | @-MAC/@-IP | famille | usage | config misc. | config réseau |
| ------ | ------ | ------ | ------ | ------ | ------ |
| @srv-ldap1 | 00:11:22:33:06:01<br />192.168.0.221 | win | ADDS+DNS | - mode UEFI<br />- DD 30Go | - 1 carte WAN br0 e1000e |
| @srv-vsphere1 | 00:11:22:33:06:02<br />192.168.0.222 | win | DPL vCenter | - mode UEFI<br />- DD 30Go | - 1 carte WAN br0 e1000e |
| @srv-esxi1 | 00:11:22:33:06:03<br />192.168.0.223 | esxi | Cluster + DD310 | - mode UEFI<br />- DD 80Go<br />- DD 320Go | - 1 carte WAN br0 e1000e |
| @srv-esxi2 | 00:11:22:33:06:04<br />192.168.0.224 | esxi | Cluster | - mode UEFI<br />- DD 80Go | - 1 carte WAN br0 e1000e |
| @srv-esxi3 | 00:11:22:33:06:05<br />192.168.0.225 | esxi | Cluster | - mode UEFI<br />- DD 80Go | - 1 carte WAN br0 e1000e |

## II) Commandes libvirt.

- `./create_vm.sh vcenter1@srv-ldap1`
- `./create_vm.sh vcenter1@srv-vsphere1` (optionnelle si déploiement vSphere par OVA/OVF)
- `./create_vm.sh vcenter1@srv-esxi1`
- `./create_vm.sh vcenter1@srv-esxi2`
- `./create_vm.sh vcenter1@srv-esxi3`

## III) Procédure.
1. **Changer** IP, DNS et hostname des ESXi (srv-esxiX) et SRV Win.
1. **Régénérer** le certificat SSL des ESXi.
1. **Désactiver** les pare-feu ou **autoriser** les ping aux win : `netsh advfirewall firewall add rule name="Enable ICMP" protocol=icmpv4:8,any dir=in action=allow`.
1. **Règler** l'heure avec ntp.org (sous windows : `w32tm /config /syncfromflags:manual /manualpeerlist:"0.fr.pool.ntp.org 1.fr.pool.ntp.org 2.fr.pool.ntp.org 3.fr.pool.ntp.org"`).
1. **Créer** dans l'AD un utilisateur "adminvsphere" dans son OU "o_adminvsphere" et son GROUP "g_adminvsphere", membre des Admins du domaine.

### Notes.

#### A) ESXi SSL.

- `cd /etc/vmware/ssl`
- `mv rui.crt rui.crt.bak mv rui.key rui.key.bak`
- `/sbin/generate-certificates`
- `ls -lia`

#### B) Fichier hosts pour l'hôte.

```
192.168.0.223	srv-esxi1.vcenter.intra
192.168.0.224	srv-esxi2.vcenter.intra
192.168.0.226	srv-vcsa.vcenter.intra
```

#### C) Menu vCenter.

![MENU](assets/img/vcenter1_menu.png)

1. h : Host
1. v : VM
1. s : Storage
1. n : Network


## IV) URLs d'accès aux interfaces.

- ESXi 1 : https://srv-esxi1.vcenter.intra/ (192.168.0.223) - user root
- ESXi 2 : https://srv-esxi2.vcenter.intra/ (192.168.0.224) - user root
- vSphere VMs : https://srv-vcsa.vcenter.intra/ (192.168.0.226) - user administrator@vsphere.local
- vSphere Management : https://srv-vcsa.vcenter.intra:5480/ (192.168.0.226:5480) - user administrator@vsphere.local

## Annexe.

### Sources tierces.

- http://www.oameri.com/vmware-deployer-un-vcsa-6-7/
- https://www.claudiokuenzler.com/blog/1274/pain-install-vmware-esxi-8-in-virtual-machine-no-network-adapter-hard-disk-found