# Labo global - global.intra.

## I) Inventaire.

note: en cours de réécriture, **SE RÉFÉRER AU SCHÉMA RÉSEAU**.

![diagram.global2.drawio.svg](assets/draw/global2.drawio.svg)

### A) Firewall bridgé, commander + netprinter.

| hostname | config&#160;réseau | config&#160;misc. | famille | usage | commande OD | fiche OD |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| @-fw | - WAN br0<br />00:11:22:33:44:01<br />192.168.0.110<br /><br />- LAN lan0<br />00:00:00:00:01:01<br />192.168.10.254<br /><br />- LAN lan1<br />00:00:00:00:01:02<br />192.168.20.254<br /><br />- DMZ dmz0<br />00:00:00:00:02:01<br />10.0.0.254 | - mode UEFI<br />- DD 10Go | bsd | Firewall | | |
| @-commander | - WAN br0<br />00:11:22:33:44:02<br />192.168.0.111<br /> | - mode UEFI<br />- DD 50Go | win | Tests WAN/VPN | `If (Test-Path 'C:\overdeploy') { Remove-Item -Path 'C:\overdeploy' -Recurse -Force \| Out-Null }; New-Item -Path 'C:\overdeploy' -itemType 'Directory' -Force \| Out-Null; Invoke-WebRequest -Uri "https://gitlab.com/fredericpetit/overdeploy-windows/-/archive/main/overdeploy-windows-main.zip" -OutFile "C:\overdeploy\overdeploy-windows-main.zip"; Expand-Archive C:\overdeploy\overdeploy-windows-main.zip -DestinationPath C:\overdeploy; Set-ExecutionPolicy Bypass -Scope Process -Force;`<br />`C:\overdeploy\overdeploy-windows\main\deploy.ps1 -profile=global_commander -mode=menu` | [fiche](https://gitlab.com/fredericpetit/overdeploy-windows/-/blob/main/data/desktop/windows_11_global_commander.json) |
| @-netprinter | - WAN br0<br />xx:xx:xx:xx:xx:xx<br />192.168.0.112<br /> | | | Tests imprimante | | |



### B) LAN (to-do).
| hostname | config&#160;réseau | config&#160;misc. | famille | usage | commande OD | fiche OD |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| @srv-ldap1 | - LAN lan0 e1000e<br />00:00:00:00:01:02<br />172.168.0.1 | - mode UEFI<br />- DD 40Go | win | ADDS+DNS | `If (Test-Path 'C:\overdeploy') { Remove-Item -Path 'C:\overdeploy' -Recurse -Force \| Out-Null }; New-Item -Path 'C:\overdeploy' -itemType 'Directory' -Force \| Out-Null; Invoke-WebRequest -Uri "https://gitlab.com/fredericpetit/overdeploy-windows/-/archive/main/overdeploy-windows-main.zip" -OutFile "C:\overdeploy\overdeploy-windows-main.zip"; Expand-Archive C:\overdeploy\overdeploy-windows-main.zip -DestinationPath C:\overdeploy; Set-ExecutionPolicy Bypass -Scope Process -Force;`<br />`C:\overdeploy\overdeploy-windows\main\deploy.ps1 -profile=fw1_globalintra -mode=menu` | [fiche](https://gitlab.com/fredericpetit/overdeploy-windows/-/blob/main/datas/server/windows_2022_fw1_globalintra.json) |
| @clt-admin1 | - LAN lan0 e1000e<br />00:00:00:00:01:03<br />172.168.0.10 | - mode UEFI<br />- DD 30Go | win | Client | `If (Test-Path 'C:\overdeploy') { Remove-Item -Path 'C:\overdeploy' -Recurse -Force \| Out-Null }; New-Item -Path 'C:\overdeploy' -itemType 'Directory' -Force \| Out-Null; Invoke-WebRequest -Uri "https://gitlab.com/fredericpetit/overdeploy-windows/-/archive/main/overdeploy-windows-main.zip" -OutFile "C:\overdeploy\overdeploy-windows-main.zip"; Expand-Archive C:\overdeploy\overdeploy-windows-main.zip -DestinationPath C:\overdeploy; Set-ExecutionPolicy Bypass -Scope Process -Force;`<br />`C:\overdeploy\overdeploy-windows\main\deploy.ps1 -profile=fw1_admin1 -mode=menu` | [fiche](https://gitlab.com/fredericpetit/overdeploy-windows/-/blob/main/datas/desktop/windows_11_fw1_admin1.json) |
| @clt-user1 | - LAN lan0 e1000e<br />00:00:00:00:01:04<br />172.168.0.20 | - mode UEFI<br />- DD 30Go | win | Client | `If (Test-Path 'C:\overdeploy') { Remove-Item -Path 'C:\overdeploy' -Recurse -Force \| Out-Null }; New-Item -Path 'C:\overdeploy' -itemType 'Directory' -Force \| Out-Null; Invoke-WebRequest -Uri "https://gitlab.com/fredericpetit/overdeploy-windows/-/archive/main/overdeploy-windows-main.zip" -OutFile "C:\overdeploy\overdeploy-windows-main.zip"; Expand-Archive C:\overdeploy\overdeploy-windows-main.zip -DestinationPath C:\overdeploy; Set-ExecutionPolicy Bypass -Scope Process -Force;`<br />`C:\overdeploy\overdeploy-windows\main\deploy.ps1 -profile=fw1_user1 -mode=menu` | [fiche](https://gitlab.com/fredericpetit/overdeploy-windows/-/blob/main/datas/desktop/windows_10_fw1_user1.json) |

### C) DMZ (to-do).
| hostname | config&#160;réseau | config&#160;misc. | famille | usage | commande OD | fiche OD |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| @srv-11 | - DMZ dmz0<br />00:00:00:00:02:02<br />10.0.0.1 | - mode UEFI<br />- DD 30Go | ubu | Apache + FTP | `apt-get update && apt-get install git -y; \`<br />`git clone --depth=1 https://gitlab.com/fredericpetit/overdeploy.git /root/overdeploy && \`<br />`chmod +x /root/overdeploy/src/linux/*.sh && chmod +x /root/overdeploy/deploy.sh; \`<br />`/bin/bash /root/overdeploy/deploy.sh --profile=web1 --mode=full; \`<br />`rm -R -f /root/overdeploy/ && history -c;` | [fiche](https://gitlab.com/fredericpetit/overdeploy-datas-example/-/blob/main/src/linux/desktop/ubuntu_2304_web1.json) |
| @srv-22 | - DMZ dmz0<br />00:00:00:00:02:03<br />10.0.0.2 |  | ubu | to-do DNS | | |
| @srv-33 | - DMZ dmz0<br />00:00:00:00:02:04<br />10.0.0.3 |  | ubu | to-do Proxy | | |
| @srv-44 | - DMZ dmz0<br />00:00:00:00:02:05<br />10.0.0.4 |  | cent | to-do Kolab | | |
| @srv-55 | - DMZ dmz0<br />00:00:00:00:02:06<br />10.0.0.5 |  | ubu | to-do Nagios | | |
| @srv-66 | - DMZ dmz0<br />00:00:00:00:02:07<br />10.0.0.6 |  | ubu | to-do Asterisk | | |

## II) Commandes libvirt.

- `./global_create_vm.sh global@commander`
- `./global_create_vm.sh global@clt-admin1`
- `./global_create_vm.sh global@clt-user1`
- `./global_create_vm.sh global@clt-user2`
- `./global_create_vm.sh global@mikael`
- `./global_create_vm.sh global@dwight`
- `./global_create_vm.sh global@jan`
- `./global_create_vm.sh global@creed`
- `./global_create_vm.sh global@jim`
- `./global_create_vm.sh global@pam`
- `./global_create_vm.sh global@andy`
- `./global_create_vm.sh global@angela`
- `./global_create_vm.sh global@erin`

## III) Procédure commentée.

- **Installer** les clients (sans configuration manuelle de leur réseau) et les serveurs (avec configuration manuelle de leur réseau).

| hostname | tâches |
| ------ | ------ |
| **@-fw** | <ul><li>**Mapper** les cartes du firewall dans l'ordre WAN, LAN0, LAN1 & DMZ (**renommer** "OPTx" par défaut).</li><li>`pfSsh.php playback enableallowallwan`, puis **activer** les interfaces, **changer** le port par défaut de pfSense, **activer** SSH, **règler** les IP fixes et DNS (resolver), **désactiver** IPV6, **rédiger** les règles, et **rédiger** les baux DHCP pour LAN & DMZ. Le DNS local sera le Firewall en lui-même, et éventuellement un autre DNS présent sur la WAN si besoin.</li></ul> |
| **@-clients** | <ul><li>Tester l'utilisation de l'imprimante réseau avec `Add-Printer -ConnectionName \\MIKAEL\netprinter`.</li></ul> |
| **@-mikael** | <ul><li>Télécharger https://gitlab.com/fredericpetit/labo/-/raw/main/misc/GPlus_PCL6_Driver_V290_32_64_00.exe .</li><li>Ajouter le driver d'imprimante : `pnputil.exe -a *.inf /subdirs /install`.</li><li>Installer le driver d'imprimante : `Add-PrinterDriver -Name "Canon Generic Plus PCL6" -InfPath "CNP60MA64.INF"`.</li><li>Ajouter un port d'imprimante : `Add-PrinterPort -Name "portprinter:" -PrinterHostAddress "192.168.0.112"`.</li><li>Ajouter une imprimante : `Add-Printer -DriverName "Canon Generic Plus PCL6" -Name "netprinter" -Shared -PortName "portprinter:"`.</li></ul> |
| **@-dwight** | **fichier(s) de conf** : _/etc/asterisk/users.conf_.<br />[ vidéo de support : https://www.youtube.com/watch?v=KABWcnut58c ] |
| **@-jan** | <ul><li>**Corriger** l'erreur `ERR_SSL_KEY_USAGE_INCOMPATIBLE` en créant un nouveau certificat.</li></ul><br /> ![CERTRDS](assets/img/global_certrds.png){width=600px} |
| **@-creed** | <ul><li>**Configurer** les hôtes à surveiller dans _/usr/local/nagios/etc/objects/_.</li></ul><br />**fichier(s) de conf** : _/usr/local/nagios/etc/nagios.cfg_.<br />[ vidéo de support : https://www.youtube.com/watch?v=EoRwOEFAmZg ] |
| **@-jim** | <ul><li>**Configurer** Apache avec les redirections vers les domaines (_global.web_ accessible depuis la WAN en tapant l'URL du Firewall - ajout dans le DNS de l'hôte), _global.help_ et _global.doc_ uniquement accessibles depuis LANx).</li></ul><br />**fichier(s) de conf** : _/etc/apache2/apache2.conf_, _/etc/proftpd/proftpd.conf_.<br />[ vidéo de support : https://www.youtube.com/watch?v=0-4y6pcALQU ] |
| **@-pam** | |
| **@-andy** | **fichier(s) de conf** : _/etc/apache2/apache2.conf_, _/etc/squid/squid.conf_.<br />[ vidéo de support : https://www.youtube.com/watch?v=pW8GYdYRt6s ] |
| **@-angela** | |
| **@-erin** | |

### Notes.

#### A) Domaines.
- _global.intra_ : 172.168.0.1 = ADDS
- _www.global.web_ : 10.0.0.1 = wordpress accessible depuis WAN net, dans /srv/www/wordpress
- _www.global.help_ : 10.0.0.1 = glpi accessible depuis LANx net, dans /home/tai/public_html
- _www.global.doc_ : 10.0.0.1 = bookstack accessible depuis LANx net, dans /home/tssr/public_html

#### B) Configuration pfSense.

![ALIASES](assets/img/global_aliases.png)

![DNS](assets/img/global_dns.png)

![DHCP](assets/img/global_dhcp.png)

![WAN](assets/img/global_wan.png)

![LAN](assets/img/global_lan.png)

![DMZ](assets/img/global_dmz.png)

![PORT](assets/img/global_port.png)

#### C) Configuration Apache.

##### /etc/apache2/sites-available/web1.conf

- https://gitlab.com/fredericpetit/overdeploy-pool-example/-/blob/main/global/apache/web1.conf
