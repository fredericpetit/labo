# Labo allin - allin.intra

## I) Inventaire.

| hostname | @-MAC/@-IP | famille | usage | configuration |
| ------ | ------ | ------ | ------ | ------ |
| @srv-fw1 | 00:11:22:33:05:01<br />192.168.0.211<br /><br />00:00:00:00:01:01<br />172.168.0.254<br /><br />00:00:00:00:02:01<br />10.0.0.254 | bsd | Firewall | - mode UEFI<br />- DD 10Go<br />- 1 carte WAN br0<br />- 1 carte LAN lan0<br />- 1 carte DMZ dmz0 |
| @srv-ldap1 | 00:00:00:00:01:02<br />172.168.0.1 | win | ADDS+DNS | - mode UEFI<br />- DD 40Go<br />- 1 carte LAN lan0 e1000e |
| @srv-router1 | | dbn | Routeur | - mode UEFI<br />- DD 20Go |
| @srv-switch1 | | dbn | Switch | - mode UEFI<br />- DD 20Go |

## II) Commandes libvirt.

- `./create_vm.sh fw2@srv-fw1`
- `./create_vm.sh fw2@srv-ldap1`
- `./create_vm.sh fw2@srv-router1`
- `./create_vm.sh fw2@srv-switch1`

## III) Procédure.
- iptables(-persistent) sous Debian pour simuleur un routeur
- Open vSwitch sous Debian pour simuler un switch